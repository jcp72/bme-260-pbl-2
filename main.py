# %%
from scipy.integrate import odeint
import time
import numpy as np
import matplotlib.pyplot as plt
import math
from plot_help import line_colors

# Values from literature
A0 = 2        #umol/hr
B0 = 2       #umol/hr
phi = -3.71     #1/hr
k_degTSH = 0.53 #1/hr
T3B = 3.4e-9 * 0.2   # M

def odefunc(y0, t):
    TSH = y0
    dTSHdt = (B0 + A0*np.sin((2*math.pi*t/24)-phi)*math.exp(-T3B)) - k_degTSH*TSH
    return dTSHdt

t = np.linspace(0, 48, 1000)
y0 = np.array([1, 1, 1])
y1 = np.array([2])
y, info = odeint(odefunc, y1, t, full_output=True)
yas = []
for i in y:
    yup = i[0]
    yas.append(yup)
T4 = []
for i in y:
    T4.append(0.442*math.log(1211/i))


displayed_time_units = "hours"
labels = ["TSH", "T4"]
Y = [yas, T4]

ax1 = plt.axes()
ax2 = ax1.twinx()
for index, y in enumerate(Y):

    line_data = [t, y, line_colors[index % len(line_colors)]]
    line_options = { "label": labels[index], "linestyle": "solid"}
    if index == 0: ax1.plot(*line_data, **line_options)
    else:  ax2.plot(*line_data, **line_options)

ax1.legend(bbox_to_anchor=(0, -0.07), loc="upper left")
ax2.legend(bbox_to_anchor=(1, -0.07), loc="upper right")
plt.subplots_adjust(bottom=0.2, right=0.85)
ax1.set_xlabel(f"Time ({displayed_time_units})")
ax1.set_ylabel("TSH Concentration (mU/L)")
ax2.set_ylabel("T4 Concentration (pmol/L)")
ax1.set_title("TSH and T4 concentrations are inversely related")
# plt.show()
# %%
plt.savefig("TSH_T4_relationship.png", dpi=300)
# %%
