import numpy as np
from final_model import run_model
from final_model import key_remember_helper as k
from final_constants import DAYS_PER_WEEK, HOURS_PER_DAY, MINS_PER_HOUR, T4_SS_CONC, TSH_SS_CONC, tsh_function_of_t4
from plot_help import PlotObject, plot
import plot_help as ph

# ph.SAVE = False

short_time = 2 * DAYS_PER_WEEK * HOURS_PER_DAY * MINS_PER_HOUR

t, y = run_model()
t_preg, y_preg = run_model(pregnant = True)
t_hypo, y_hypo = run_model(thyroid_activity_factor = 0.8, total_time_minutes = short_time)
t_no_pituitary, y_no_pituitary = run_model(thyroid_activity_factor = 0.8, disable_pituitary=True, total_time_minutes = short_time)

t4 = np.linspace(1e-12, 30e-12, 1000)
tsh = tsh_function_of_t4(t4)

ph.plot_count = 0 ; plot(t_preg, [y_preg], [k.hCG], title="[hCG] rises dramatically during the course of pregnancy").done()
ph.plot_count = 1 ; plot(t_preg, [y_preg], [k.RTSH, k.RhCG], [k.hCG], title="Rise in [hCG] causes decreased binding of TSH to TSHR").done()
ph.plot_count = 9 ; plot(t_preg, [y, y_preg], [k.plasma_FT3, k.plasma_FT4], title="Pregnancy causes net changes in T3 and T4", descriptions=["normal", "pregnant"]).done()

ph.plot_count = 2 ; plot(t, [y], [k.TSH], [k.plasma_FT3, k.plasma_FT4], title = "No change with steady state starting condition").done()
# plot(t_preg, [y_preg], [k.T4_transferred_placenta]).done()
ph.plot_count = 3 ; plot(t_hypo, [y, y_hypo], [k.plasma_FT3], [k.TSH], title="Hypothyroidism results in increased TSH generation", descriptions=["normal", "hypo"]).done()
ph.plot_count = 4 ; plot(t_hypo, [y_hypo, y_no_pituitary], [k.RTSH], [k.plasma_FT4], descriptions = ["normal hypo.", "hypo. w/o pituitary"], title="Release of TSH is a critical response to hypothyroidism", start_index = 10).done()

ph.plot_count = 5 ; plot(t, [y, y_hypo], [k.T3_store_exchange, k.T4_store_exchange], descriptions = ["normal", "hypo"], title="Hypothyroidism results in exchange of T3, T4 with storage").done()
ph.plot_count = 6 ; plot(t, [y, y_hypo], [k.T4_net_thyroid], descriptions = ["normal", "hypo"], title="Overall T4 production decreases in hypothyroidism").done()

def fix_t4_plot(po: PlotObject):
    po.plt.xlabel("T4 Concentration (M)")
    po.plt.ylabel("TSH Concentration (M)")
    p = po.plt.plot(T4_SS_CONC, TSH_SS_CONC, "o") 
    po.plt.legend(p, ["Steady-state values"])
ph.plot_count = 8 ; plot(t4, [{"TSH": tsh}], ["TSH"], title="TSH responds inversely to T4 concentrations", displayed_time_units = "minutes").modify(fix_t4_plot)