import numpy as np
from scipy.integrate import odeint
from final_constants import *
from final_model_map import *

def run_model(pregnant = PREGNANT, thyroid_activity_factor = 1.0, 
        total_time_minutes = TOTAL_TIME_MINUTES, disable_pituitary = False,
        halt_nonzero_differential = False):

    it_count = 0

    def odefunc(y, t):

        nonlocal it_count

        values = get_dict_from_y_set(y)
        for name in values: globals()[name] = values[name] # This is so we don't 
                                                           # have to worry about 
                                                           # the ordering of 
                                                           # variables in more 
                                                           # than one place.

        target_TSH_conc = tsh_function_of_t4(T4)

        f = (RTSH + RhCG) / receptor_count

        if disable_pituitary:
            f = ss_fraction_tshr_bound

        # DIFFERENTIALS

        R_binding_TSH = - kr_TSH * RTSH + kf_TSH * R * TSH
        R_binding_hCG = - kr_hCG * RhCG + kf_hCG * R * hCG

        TSH_binding = R_binding_TSH * moles_cells_per_liter
        TSH_generation_pituitary = (target_TSH_conc - TSH) * TSH_SPEED
        
        T2_consumption_thyroid = k_T3 * f * T2 * thyroid_activity_factor

        T2_to_KL   = k_to_KL * T2
        T2_from_KL = k_from_KL * T2_KL

        T3_generation_thyroid = T2_consumption_thyroid
        T3_consumption_thyroid = k_T4 * f * T3 * thyroid_activity_factor

        T3_storage = k_storage * T3
        T3_recycle = k_recycle * T3_other

        T4_generation_thyroid = T3_consumption_thyroid
        
        T4_consumption_KL = r3 * T4_KL

        T3_generation_KL = T4_consumption_KL
        T3_consumption_KL = r2 * T3_KL

        T2_generation_KL = T3_consumption_KL

        T4_storage = k_storage * T4
        T4_recycle = k_recycle * T4_other

        T4_to_KL   = k_to_KL * T4
        T4_from_KL = k_from_KL * T4_KL

        hCG_generation_due_to_pregnancy = .00923 / (12 * DAYS_PER_WEEK * 
            HOURS_PER_DAY * MINS_PER_HOUR / 2000) * hCG * (1 - 
            (hCG / hCG_conversion_factor / 290000))
        hCG_binding = R_binding_hCG * moles_cells_per_liter

        T4_loss_to_placenta = (kp * T4_other) if pregnant else 0
        T3_loss_to_placenta = (kp * T3_other) if pregnant else 0

        if disable_pituitary:
            TSH_generation_pituitary *= 1e-10 # Really this should be zero,
                                              # but any smaller and odeint 
                                              # forgets how to integrate.

        differentials = {

            ### TRACKED TERMS (non-accumulating) ###

            "R":        - R_binding_TSH - R_binding_hCG,
            "TSH":      - TSH_binding + TSH_generation_pituitary,
            "RTSH":       R_binding_TSH,
            "T2":       - T2_consumption_thyroid + (T2_from_KL - T2_to_KL),

            "T3":         T3_generation_thyroid - T3_consumption_thyroid + (
                T3_recycle - T3_storage),

            "T4":         T4_generation_thyroid - T4_loss_to_placenta + (
                T4_recycle - T4_storage) + (T4_from_KL - T4_to_KL),

            "hCG":      - hCG_binding + hCG_generation_due_to_pregnancy,
            "RhCG":       R_binding_hCG,
            "T3_other":  (T3_storage - T3_recycle) * V_thyroid / V_other \
                - T3_loss_to_placenta,
            "T4_other":  (T4_storage - T4_recycle) * V_thyroid / V_other \
                - T4_loss_to_placenta,
            "T2_KL":     (T2_to_KL - T2_from_KL) * V_thyroid / V_KL + (
                T2_generation_KL),
            "T3_KL":      T3_generation_KL - T3_consumption_KL,
            "T4_KL":    (T4_to_KL - T4_from_KL) * V_thyroid / V_KL - (
                T4_consumption_KL)
        }
        
        accumulating_differentials = {

            ### ACCUMULATING TERMS (used to find rates of subprocesses) ###

            "TSH_generation_pituitary": TSH_generation_pituitary, 

            "T2_KL_exchange": T2_from_KL - T2_to_KL,
            "T2_net_thyroid": T2_consumption_thyroid,
            "T2_net_KL": T2_generation_KL,

            "T3_store_exchange": T3_recycle - T3_storage,
            "T3_KL_exchange": 0, # maybe fix later
            "T3_net_thyroid": T3_generation_thyroid - T3_consumption_thyroid,
            "T3_net_KL": T3_generation_KL - T3_consumption_KL,

            "T4_store_exchange": T4_recycle - T4_storage,
            "T4_KL_exchange": T4_from_KL - T4_to_KL,
            "T4_net_thyroid": T4_generation_thyroid,
            "T4_net_KL": T4_consumption_KL,
            "T4_transferred_placenta": T4_loss_to_placenta

        }

        if halt_nonzero_differential:
            for key in differentials:
                if differentials[key] > 1e-15 and not key.startswith("R"):
                    print(differentials)
                    raise ValueError(f"Iteration = {it_count}, " 
                        f"Differential {key} = {differentials[key]}")
            it_count += 1

        differentials.update(accumulating_differentials) # add the accumulating
                                                         # differentials to the
                                                         # dict

        return create_list_from_single_values(differentials)

    initial_values = {

        "R":        receptor_count * (1 - ss_fraction_tshr_bound), 
        "TSH":      TSH_SS_CONC, 
        "RTSH":     receptor_count * ss_fraction_tshr_bound, 
        "T2":       T2_SS_CONC, 
        "T3":       T3_SS_CONC, 
        "T4":       T4_SS_CONC, 
        "hCG":      hCG_0 if pregnant else 0, 
        "RhCG":     0,
        "T3_other": T3_SS_CONC,
        "T4_other": T4_SS_CONC,
        "T2_KL":    T2_KL_SS_CONC,
        "T3_KL":    T3_KL_SS_CONC,
        "T4_KL":    T4_SS_CONC,

        ### ACCUMULATION

        "TSH_generation_pituitary": 0,

        "T2_KL_exchange": 0,
        "T2_net_thyroid": 0,
        "T2_net_KL": 0,

        "T3_store_exchange": 0,
        "T3_KL_exchange": 0,
        "T3_net_thyroid": 0,
        "T3_net_KL": 0,

        "T4_store_exchange": 0,
        "T4_KL_exchange": 0,
        "T4_net_thyroid": 0,
        "T4_net_KL": 0,

        "T4_transferred_placenta": 0

    } 

    Y0 = create_list_from_single_values(initial_values)

    t = np.linspace(0, total_time_minutes, STEPS)
    y_result = odeint(odefunc, Y0, t)

    results_dict = get_dict_from_odeint_result(y_result)

    important_results = {
        "plasma_FT4": results_dict["T4"],
        "plasma_FT3": [v / 2 for v in results_dict["T4"]],
        "plasma_TT4": [v * Cf_FREE_TO_TOTAL_T4 for v in results_dict["T4"]],
        "plasma_TT3": [v * Cf_FREE_TO_TOTAL_T3 for v in results_dict["T4"]],
    }

    results_dict.update(important_results)

    return t, results_dict

if __name__ == "__main__":
    # Still run the main script even if you accidentally run this script
    from final_analysis import *
