from final_model_map import properties_of
import matplotlib.pyplot as plt
from final_constants import *
import warnings, os
from re import sub
from datetime import datetime

SAVE = True

plt.style.use('seaborn-bright')
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = ["Arial"]
plt.rcParams["font.size"] = 12
plt.rcParams["figure.figsize"] = (8.53, 4.8)

plot_count = 0

def snake_case(s):
    """
    Modified from 
    https://www.w3resource.com/python-exercises/string/
    python-data-type-string-exercise-97.php
    """
    return sub("[^A-Za-z0-9_]", "", '_'.join(
        sub('([A-Z][a-z]+)', r' \1',
        sub('([A-Z]+)', r' \1',
        s.replace('-', ' '))).split()
    ).lower())

line_colors = [
    "#E69F00", "#CC5997", "#009E73", "#0072B2", "#56B4E9", "#D55E00", 
    "#777777", "#000000"
]

graph_color_indices = {

    "R"   :     0,
    "TSH" :     1,
    "RTSH":     1,
    "T2"  :     3,
    "T3"  :     4,
    "T4"  :     2,
    "hCG" :     0,
    "RhCG":     2,
    "T3_other": 4,
    "T4_other": 5,
    "T2_KL":    3,
    "T3_KL":    4,
    "T4_KL":    5,

    ### ACCUMULATION

    "TSH_generation_pituitary": 0,

    "T2_KL_exchange":           0,
    "T2_net_thyroid":           1,
    "T2_net_KL":                2,

    "T3_store_exchange":        0,
    "T3_KL_exchange":           1,
    "T3_net_thyroid":           2,
    "T3_net_KL":                3,

    "T4_store_exchange":        1,
    "T4_KL_exchange":           2,
    "T4_net_thyroid":           3,
    "T4_net_KL":                4,
    "T4_transferred_placenta":  5,

    # OTHER

    "plasma_FT4": 2,
    "plasma_FT3": 4,
    "plasma_TT4": 2,
    "plasma_TT3": 4
}

graph_labels_nice = {

    "R"   :     "R",
    "TSH" :     "TSH",
    "RTSH":     "RTSH",
    "T2"  :     "T2",
    "T3"  :     "T3",
    "T4"  :     "T4",
    "hCG" :     "hCG",
    "RhCG":     "RhCG",
    "T3_other": "T3 (other tissues)",
    "T4_other": "T4 (other tissues)",
    "T2_KL":    "T2_KL",
    "T3_KL":    "T3_KL",
    "T4_KL":    "T4_KL",

    ### ACCUMULATION

    "TSH_generation_pituitary": 0,

    "T2_KL_exchange":           "T2 in/out, KL",
    "T2_net_thyroid":           "T2 gen/con, thyroid",
    "T2_net_KL":                "T2 gen/con, KL",

    "T3_store_exchange":        "T3 in/out, storage",
    "T3_KL_exchange":           "T3 in/out, KL",
    "T3_net_thyroid":           "T3 gen/con, thyroid",
    "T3_net_KL":                "T3 gen/con, KL",

    "T4_store_exchange":        "T4 in/out, storage",
    "T4_KL_exchange":           "T4 in/out, KL",
    "T4_net_thyroid":           "T4 gen/con, thyroid",
    "T4_net_KL":                "T4 gen/con, KL",
    "T4_transferred_placenta":  "T4 out, to placenta",

    # OTHER

    "plasma_FT4": "Free T4",
    "plasma_FT3": "Free T3",
    "plasma_TT4": "Total T4",
    "plasma_TT3": "Total T3"
}

class PlotObject:
    def __init__(self, plt: plt, ax1: plt.Axes, ax2: plt.Axes, title: str):
        self.plt = plt
        self.ax1 = ax1
        self.ax2 = ax2
        self.title = title
    def done(self):
        """
        Saves or shows the plot.
        """
        if SAVE:
            plt.savefig(os.path.join("plots",
                f"{plot_count + 1:03d}_" + 
                # f"{'{:%Y%m%d_%H%M%S}'.format(datetime.now())}_" + 
                f"{snake_case(self.title)}"), 
                dpi=300)
        else:
            plt.show()
        plt.close()
    def modify(self, function_name):
        function_name(self) # Run `function_name` to modify this plot.
        self.done()         # Then save the plot.

def plot(t, datasets, ax1_keys, ax2_keys = [], title="", 
        descriptions = ["", "", ""], displayed_time_units = 
        displayed_time_units, start_index = 0):
    """
    Supports plotting up to three sets of many lines. Each set has a 
    description and corresponds to a single run of the model, and the sets 
    will be graphed with solid, dashed, and dotted lines, respectively.
    """

    plt.clf()
    
    ax1 = plt.axes()
    
    use_ax2 = len(ax2_keys) > 0

    if use_ax2:
        ax2 = ax1.twinx()
    else:
        ax2 = None

    time_divisor = time_conversions[displayed_time_units]
    scaled_time = t / time_divisor

    ax1_units = properties_of[ax1_keys[0]].units.base_units
    
    if use_ax2: ax2_units = properties_of[ax2_keys[0]].units.base_units

    line_styles = ["solid", "dashed", "dotted"]

    for index, data in enumerate(datasets):

        for key in ax1_keys:
            ax1.plot(scaled_time[start_index:], data[key][start_index:], 
                line_colors[graph_color_indices[key]],
                linestyle = line_styles[index],
                label = graph_labels_nice[key] + (
                    f" ({descriptions[index]})" 
                    if descriptions[index] != "" 
                    else "")
                )
            if properties_of[key].units.base_units != ax1_units:
                warnings.warn("Units for axis 1 do not match!")

        for key in ax2_keys:
            ax2.plot(scaled_time[start_index:], data[key][start_index:], 
                line_colors[graph_color_indices[key]],
                linestyle = line_styles[index],
                label = graph_labels_nice[key] + (
                    f" ({descriptions[index]})" 
                    if descriptions[index] != "" 
                    else "")
                )
            if properties_of[key].units.base_units != ax2_units:
                warnings.warn("Units for axis 2 do not match!")

    ax1.legend(bbox_to_anchor=(-0.1, -0.07), loc="upper left")

    if use_ax2: ax2.legend(bbox_to_anchor=(1.1, -0.07), loc="upper right")
    
    plt.subplots_adjust(bottom=0.3, right=0.85)

    ax1.set_xlabel(f"Time ({displayed_time_units})")

    ax1.set_ylabel(ax1_units)
    if use_ax2: ax2.set_ylabel(ax2_units)

    plt.title(title)

    return PlotObject(plt, ax1, ax2, title)
