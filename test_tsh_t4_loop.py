from math import exp, log
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from util import reformat_columnwise
from plot_help import line_colors

T4_SS_CONC =  15.0e-12   # M 
T3_SS_CONC =   3.4e-09   # M
T2_SS_CONC = 250.0e-12   # M

THYR_METAB_RATE = 1e-2
CELLS_PER_L = 4e9
RECEPTORS_PER_CELL = 1e3

PLASMA_I_CONC = 1

TSH_SS_CONC = 20e-12 # M
TSH_HALF_LIFE = 50 # min

VOLUME = 1 # L
nA = 6.02e23

### DANGER ZONE


ss_frac_bound = lambda L: L / (kr_TSH / kf_TSH + L)

kd = log(2) / (6 * 1440)
k_half_TSH = log(2) / 60

kf_TSH = 2.16e8 
kr_TSH = 2.16e-1


kf_hCG = 2.16e8 / 5e7 * 0.001 # assume 3e6x weaker binding 
kr_hCG = 2.16e-1      * 0.001 # and * 0.001 because hCG is measured in mM rather than M

ss_fraction_tshr_bound = ss_frac_bound(TSH_SS_CONC)

# Highly questionable zone

k4 = 1e10 # Semi-arbitrary
TSH_SPEED = 1e3

A = TSH_SS_CONC * (exp(k4 * T4_SS_CONC))

tshr_fraction_function = lambda frac: frac
R_T4genMax = kd * T4_SS_CONC / tshr_fraction_function(ss_fraction_tshr_bound)

PREGNANT = True

################################################################################

receptor_count = RECEPTORS_PER_CELL
conversion_factor = nA / CELLS_PER_L

k_T4 = R_T4genMax / T3_SS_CONC
k_T3 = T3_SS_CONC / T2_SS_CONC * k_T4

hCG_conversion_factor = 2.35e-9 # WARNING: measured in mmol / L !!! Measuring in mol / L is too small and causes odeint to FAIL.
hCG_0 = 5 * hCG_conversion_factor # U/L --> mmol / L

labels = ["R", "TSH", "RTSH", "T2", "T3", "T4", "hCG", "RhCG"]

# TIMING

DAYS_PER_WEEK = 7
HOURS_PER_DAY = 24
MINS_PER_HOUR = 60

time_conversions = {
    "weeks"  : MINS_PER_HOUR * HOURS_PER_DAY * DAYS_PER_WEEK,
    "days"   : MINS_PER_HOUR * HOURS_PER_DAY,
    "hours"  : MINS_PER_HOUR,
    "minutes": 1
}

weeks = 12

total_time_minutes = weeks * DAYS_PER_WEEK * HOURS_PER_DAY * MINS_PER_HOUR
# total_time_minutes = 60000 # hypothyroidism, non-pregnant
displayed_time_units = "days"

def odefunc(y, t):

    R, TSH, RTSH, T2, T3, T4, hCG, RhCG = y

    target_TSH_conc = A * exp(-k4 * T4)

    fraction_tshr_bound = (RTSH + RhCG) / receptor_count
    # fraction_tshr_bound = ss_fraction_tshr_bound

    f = tshr_fraction_function(fraction_tshr_bound)

    thyroid_activity_factor = 1.0

    dR_dt    =   kr_TSH * RTSH - kf_TSH * R * TSH + kr_hCG * RhCG - kf_hCG * R * hCG
    dTSH_dt  =  (kr_TSH * RTSH - kf_TSH * R * TSH) / conversion_factor + (target_TSH_conc - TSH) * TSH_SPEED
    dRTSH_dt = - kr_TSH * RTSH + kf_TSH * R * TSH
    dT2_dt   = - k_T3 * f * T2 * thyroid_activity_factor + kd * T4
    dT3_dt   =   k_T3 * f * T2 * thyroid_activity_factor - k_T4 * f * T3 * thyroid_activity_factor
    dT4_dt   = - kd * T4 + k_T4 * f * T3 * thyroid_activity_factor
    dhCG_dt  = .00923 / (12 * DAYS_PER_WEEK * HOURS_PER_DAY * MINS_PER_HOUR / 2000) * hCG * (1 - (hCG / hCG_conversion_factor / 290000)) + (kr_hCG * RhCG - kf_hCG * R * hCG) / conversion_factor
    dRhCG_dt = - kr_hCG * RhCG + kf_hCG * R * hCG

    return [
        dR_dt,
        dTSH_dt,
        dRTSH_dt,
        dT2_dt,
        dT3_dt,
        dT4_dt,
        dhCG_dt,
        dRhCG_dt
    ]

# added initial hCG concentration as an input
Y0 = [ receptor_count * (1 - ss_fraction_tshr_bound), TSH_SS_CONC, receptor_count * ss_fraction_tshr_bound, 
        T2_SS_CONC, T3_SS_CONC, T4_SS_CONC, hCG_0 if PREGNANT else 0, 0] 

t = np.linspace(0, total_time_minutes, 100000)
y_result = odeint(odefunc, Y0, t)

Y = reformat_columnwise(y_result)

for i in [0, 2, 7]:
    # convert from # / cell to mol / L, except for index 4 which is L
    for j in range(len(Y[i])):
        Y[i][j] /= conversion_factor 
        Y[i][j] = round(Y[i][j], 20) # round to 20 decimal places

ax1 = plt.axes()
ax2 = ax1.twinx()

for index, y in enumerate(Y):

    time_divisor = time_conversions[displayed_time_units]

    line_data = [t / time_divisor, y, line_colors[index % len(line_colors)]]
    line_options = { "label": labels[index], "linestyle": "solid" if index < len(line_colors) else "dashed" }

    if index < 3 and index > 0:
        if index == 1: 
            pass
            line_data[1] = [l / 100 for l in line_data[1]]
            # ax1.plot(*line_data, **line_options)
        else:
            ax1.plot(*line_data, **line_options)
    else:
        if index == 5:
            pass
            ax2.plot(*line_data, **line_options)
        # ax2.plot(*line_data, **line_options)
        if index == 6:
            pass
            # ax1.plot(*line_data, **line_options)
        if index == 7:
            pass
            ax1.plot(*line_data, **line_options)

ax1.legend(bbox_to_anchor=(0, -0.07), loc="upper left")
ax2.legend(bbox_to_anchor=(1, -0.07), loc="upper right")
plt.subplots_adjust(bottom=0.3, right=0.85)
ax1.set_xlabel(f"Time ({displayed_time_units})")
ax1.set_ylabel("Concentration (M)")
ax2.set_ylabel("Concentration (M)")
# ax1.set_yscale("log")
# ax1.hlines([1.25e-12 / 2, 1.25e-12], 0, 25)
plt.show()
