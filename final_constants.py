from math import log
from numpy import exp

STEPS = 10000

T4_SS_CONC =  10.0e-12   # M, unbound
T3_SS_CONC =   5.0e-12   # M, unbound
T2_SS_CONC = 250.0e-12   # M

Cf_FREE_TO_TOTAL_T4 = 9000
Cf_FREE_TO_TOTAL_T3 = 800

V_other = 54
V_thyroid = 0.0085

V_KL = 1.4

CELLS_PER_L = 4e9
RECEPTORS_PER_CELL = 1e5

TSH_SS_CONC = 20e-12 # M

nA = 6.02e23

ss_frac_bound = lambda L: L / (kr_TSH / kf_TSH + L)

r3 = log(2) / (6 * 1440)
k_half_TSH = log(2) / 60

kf_TSH = 2.16e9
kr_TSH = 2.16e-1

kf_hCG = kf_TSH / 1e7 * 0.001 # assume 10,000,000x weaker binding 
kr_hCG = kr_TSH       * 0.001 # and * 0.001 because hCG is measured in mM, not M

ss_fraction_tshr_bound = ss_frac_bound(TSH_SS_CONC)

k4 = 5 / T4_SS_CONC # Semi-arbitrary, needs to balance magnitude of T4
TSH_SPEED = 1e3

A = TSH_SS_CONC * (exp(k4 * 0.2 * T4_SS_CONC))

R_T4genMax = (V_KL / V_thyroid) * r3 * T4_SS_CONC / ss_fraction_tshr_bound

PREGNANT = False

################################################################################

receptor_count = RECEPTORS_PER_CELL
moles_cells_per_liter = CELLS_PER_L / nA

k_T4 = R_T4genMax / T3_SS_CONC
k_T3 = T3_SS_CONC / T2_SS_CONC * k_T4

hCG_conversion_factor = 2.35e-9 # WARNING: measured in mmol / L
hCG_0 = 5 * hCG_conversion_factor # U/L --> mmol / L

# TIMING

DAYS_PER_WEEK = 7
HOURS_PER_DAY = 24
MINS_PER_HOUR = 60

time_conversions = {
    "weeks"  : MINS_PER_HOUR * HOURS_PER_DAY * DAYS_PER_WEEK,
    "days"   : MINS_PER_HOUR * HOURS_PER_DAY,
    "hours"  : MINS_PER_HOUR,
    "minutes": 1
}

weeks = 12

TOTAL_TIME_MINUTES = weeks * DAYS_PER_WEEK * HOURS_PER_DAY * MINS_PER_HOUR
displayed_time_units = "days"

k_storage = 3e-2
k_recycle = 3e-2

k_to_KL   = 2
k_from_KL = k_to_KL - (V_KL / V_thyroid) * r3

# ENZYME CONSTANTS

T2_KL_SS_CONC = (k_T3 * ss_fraction_tshr_bound + k_to_KL) * T2_SS_CONC / (
    k_from_KL)
r2 = r3 * T4_SS_CONC / (k_from_KL * T2_KL_SS_CONC - k_to_KL * T2_SS_CONC) * (
    V_KL / V_thyroid)
T3_KL_SS_CONC = r3 / r2 * T4_SS_CONC

### TSH FUNCTION

tsh_function_of_t4 = lambda T4: A * exp(-k4 * 0.2 * T4)

### PLACENTA

kp = 1.9e-7
V_placenta = 0.3

if __name__ == "__main__":
    # Still run the main script even if you accidentally run this script
    from final_analysis import *
