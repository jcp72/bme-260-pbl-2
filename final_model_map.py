from numpy import ndarray
from final_constants import RECEPTORS_PER_CELL, V_KL, V_other, V_thyroid, \
    moles_cells_per_liter
from util import reformat_columnwise

class Units:
    def __init__(self, conversion_factor: float, base_units: "str", 
            should_round: bool = False):
        """
        Note, `conversion_factor` is the value by which the given value in 
        `Unit`s should be multiplied to reach `base_units`. 

        Example: for millimeters, the conversion factor is 0.001 for a 
                 base unit of "meters".
        """
        self.conversion_factor = conversion_factor
        self.base_units = base_units
        self.should_round = should_round

UNITS_count_per_cell = Units(moles_cells_per_liter, "Concentration (M)")
UNITS_count_per_cell = Units(1 / RECEPTORS_PER_CELL, r"fraction of receptors")
UNITS_mM = Units(0.001, "Concentration (M)")
UNITS_M = Units(1, "Concentration (M)")

UNITS_M_in_thyroid = Units(V_thyroid, "Net amount generated (mol)")
UNITS_M_in_KL = Units(V_KL, "Net amount generated (mol)")
UNITS_M_en_route = Units(V_thyroid, "Net amount transferred (mol)")
UNITS_M_en_route_from_other = Units(V_other, "Net amount transferred (mol)")

class DataSetProperties:
    def __init__(self, name: str, units: Units):
        """
        Note, `units` should be the units used in the integration. They 
        """
        self.name = name
        self.units = units

dsp = DataSetProperties # create alias

tracked_data = [

    dsp("R"   ,     UNITS_count_per_cell),
    dsp("TSH" ,     UNITS_M),
    dsp("RTSH",     UNITS_count_per_cell),
    dsp("T2"  ,     UNITS_M),
    dsp("T3"  ,     UNITS_M),
    dsp("T4"  ,     UNITS_M),
    dsp("hCG" ,     UNITS_mM),
    dsp("RhCG",     UNITS_count_per_cell),
    dsp("T3_other", UNITS_M),
    dsp("T4_other", UNITS_M),
    dsp("T2_KL",    UNITS_M),
    dsp("T3_KL",    UNITS_M),
    dsp("T4_KL",    UNITS_M),
    
    ### ACCUMULATION

    dsp("TSH_generation_pituitary", UNITS_M_in_thyroid),

    dsp("T2_KL_exchange", UNITS_M_en_route),
    dsp("T2_net_thyroid", UNITS_M_in_thyroid),
    dsp("T2_net_KL", UNITS_M_in_KL),

    dsp("T3_store_exchange", UNITS_M_en_route),
    dsp("T3_KL_exchange", UNITS_M_en_route),
    dsp("T3_net_thyroid", UNITS_M_in_thyroid),
    dsp("T3_net_KL", UNITS_M_in_KL),

    dsp("T4_store_exchange", UNITS_M_en_route),
    dsp("T4_KL_exchange", UNITS_M_en_route),
    dsp("T4_net_thyroid", UNITS_M_in_thyroid),
    dsp("T4_net_KL", UNITS_M_in_KL),

    dsp("T4_transferred_placenta", UNITS_M_en_route_from_other),

    # OTHER

    dsp("plasma_FT4", UNITS_M),
    dsp("plasma_FT3", UNITS_M),
    dsp("plasma_TT4", UNITS_M),
    dsp("plasma_TT3", UNITS_M)

]

properties_of = {
    data_properties.name: data_properties for data_properties in tracked_data
}

def create_list_from_single_values(data: dict):
    return [ data[dataset.name] for dataset in tracked_data[:-4]] 
                                                   # -4 is because the
                                                   # plasma concentrations are
                                                   # calculated after the model 
                                                   # has finished running

def get_dict_from_odeint_result(result: ndarray):
    Y = reformat_columnwise(result)
    return {
        props.name:
            Y[index] 
                if props.units.conversion_factor == 1 
                and not props.units.should_round
            else [
                round(Y[index][j] * props.units.conversion_factor, 20)
                for j in range(len(Y[index]))
            ]
        for index, props in enumerate(tracked_data[:-4])
    }

def get_dict_from_y_set(y: ndarray):
    return { props.name: y[index] for index, props in enumerate(
        tracked_data[:-4])}


# The below line is only to avoid IDE warnings. 
R = TSH = RTSH = T2 = T3 = T4 = hCG = RhCG = T3_other = T4_other = T2_KL = \
T3_KL = T4_KL = 0

class key_remember_helper():

    # Helps to remember what the dictionary keys are. Again, an IDE helper.

    R = "R"
    TSH = "TSH"
    RTSH = "RTSH"
    T2 = "T2"
    T3 = "T3"
    T4 = "T4"
    hCG = "hCG"
    RhCG = "RhCG"
    T3_other = "T3_other"
    T4_other = "T4_other"
    T2_KL = "T2_KL"
    T3_KL = "T3_KL"
    T4_KL = "T4_KL"
    
    ### ACCUMULATION
    
    TSH_generation_pituitary = "TSH_generation_pituitary"

    T2_KL_exchange = "T2_KL_exchange"
    T2_net_thyroid = "T2_net_thyroid"
    T2_net_KL = "T2_net_KL"

    T3_store_exchange = "T3_store_exchange"
    T3_KL_exchange = "T3_KL_exchange"
    T3_net_thyroid = "T3_net_thyroid"
    T3_net_KL = "T3_net_KL"

    T4_store_exchange = "T4_store_exchange"
    T4_KL_exchange = "T4_KL_exchange"
    T4_net_thyroid = "T4_net_thyroid"
    T4_net_KL = "T4_net_KL"
    T4_transferred_placenta = "T4_transferred_placenta"

    plasma_FT4 = "plasma_FT4"
    plasma_FT3 = "plasma_FT3"
    plasma_TT4 = "plasma_TT4"
    plasma_TT3 = "plasma_TT3"
