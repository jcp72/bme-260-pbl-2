def reformat_columnwise(l):
    """
    Reorganizes a list so that it is transformed from this:
    
        [A(0), B(0), C(0)],
        [A(1), B(1), C(1)],
        ...
        [A(n), B(n), C(n)]
    
    into this:

        [A(0), A(1), ..., A(n)],
        [B(0), B(1), ..., B(n)],
        [C(0), C(1), ..., C(n)]

    """
    return [[l[i][j] for i in range(len(l))] for j in range(len(l[0]))]